# -*- coding: utf-8 -*-
"""
load_traj_save_pickle.py
The purpose of this script is to load the trajectory and save all important information in a form of a pickle
The idea here is that we can use the pickle information to do subsequent analysis without having MDTraj installed onto your computer

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)
    
WRITTEN ON: 11/08/2018
"""
### IMPORTING MODULES
import mdtraj as md
import MDDescriptors.core.import_tools as import_tools # Loading trajectory details

### IMPORTING MATH FUNCTION
import numpy as np

### IMPORTING PICKLE
import pickle



#%% MAIN SCRIPT
if __name__ == "__main__":
    ## DEFINING TRAJECTORY INFORMATION
    analysis_dir=r"EAM_SPHERICAL_HOLLOW" # Analysis directory
    category_dir="EAM" # category directory
    specific_dir="EAM_310.15_K_4_nmDIAM_octanethiol_CHARMM36_Trial_1" 
    specific_dir="EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1"  
    
    ## DEFINING FULL PATH
    path2AnalysisDir=r"R:\scratch\nanoparticle_project\analysis\\" + analysis_dir + '\\' + category_dir + '\\' + specific_dir + '\\' # PC Side
    
    ## DEFINING FILE NAMES
    gro_file=r"sam_prod_10_ns_whole_no_water_center.gro" # Structural file
    xtc_file=r"sam_prod_10_ns_whole_no_water_center.xtc" # r"sam_prod_10_ns_whole.xtc" # Trajectory file

    ## DEFINING OUTPUT PATH
    output_pickle_path=r"C:\Users\akchew\cluster_tutorial"
        
    ## DEFINING FULL PATH
    gro_path = path2AnalysisDir + '/' + gro_file
    xtc_path = path2AnalysisDir + '/' + xtc_file
    
    ## LOADING TRAJECTORY
    traj = md.load(xtc_path, top=gro_path)
    #%%
    ############################################
    ### EXTRACTION OF TRAJECTORY INFORMATION ###
    ############################################
    
    ## DEFINING OUTPUT PICKLE NAME
    pickle_name="traj_last_frame.pickle" # 10_
    
    ## PICKLE PATH
    pickle_path = output_pickle_path + '/' + pickle_name
    
    ## DEFINING TRAJECTORY RANGE THAT YOU CARE ABOUT
    trajectory_range=-1 # np.arange(-10,-1 + 1) #  #   # -1 # Last frame
    
    ## DEFINING TRAJECTORY
    traj = traj[trajectory_range] # traj_data.traj[trajectory_range]
    
    ## DEFINING POSITIONS
    atom_positions = np.array(traj.xyz[:])
    
    ## DEFINING ATOM NAMES
    atom_names = [ each_atom.name for each_atom in traj.topology.atoms ]
    
    ## DEFINING RESIDUE NAMES
    residue_names = [ each_residue.name for each_residue in traj.topology.residues]
    
    ## DEFINING ATOM AND CORRESPONDING RESIDUE
    residue_atom_list = [ [ each_atom.residue.name, each_atom.name, each_atom.element.symbol ] for each_atom in traj.topology.atoms ]
    # ELEMENT: Atom index, name of element, element symbol, atomic mass, atomic radius
    
    ## FINDING BOX DIMENSIONS
    box_dimensions = traj.unitcell_lengths
    
    ## SAVING AS A PICKLE
    with open(pickle_path, 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([atom_positions, residue_atom_list, box_dimensions], f, protocol=2)  # <-- protocol 2 required for python2   # -1 # residue_atom_list
    
    ## PRINTING
    print("GENERATED PICKLE AT: %s"%(pickle_path) )
    
    #%%
    # To load the data:
    ## LOADING THE DATA
    with open(pickle_path,'rb') as f:
        atom_positions, residue_atom_list, box_dimensions = pickle.load(f)
    
    
    
    