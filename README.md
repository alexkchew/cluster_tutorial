# cluster_tutorial
The purpose of this project is to provide a clustering tutorial for students interested in using clustering algorithms for their work. In this tutorial, we will go over using heirarchical density-based spatial clustering of applications with noise (HDBSCAN) and its former DBSCAN. These clustering algorithms were used to find ligands that are bundled together, described in greater detail here: https://pubs.acs.org/doi/10.1021/acs.jpcc.8b09323

The jupyter notebook used here computes bundling groups for a 4 nm faceted gold core with hexadecanethiol ligands. 

# Files
- clustering_tutorial.ipynb: main jupyter notebook tutorial
- load_traj_save_pickle.py: python script used to generate pickle from MDTraj
- traj_last_frame.pickle: pickle containing one frame of 4 nm faceted gold core hexadecanethiol ligands

# Notes
- Please use clustering_tutorial.ipynb by opening this directory and typing "jupyter notebook" in the command line
- This tutorial requires downloading "hdbscan" module. You can do that by "pip install hdbscan" or "conda install -c conda-forge hdbscan"
